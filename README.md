Deeplearning4Spark: Distributed Neural Net Platform (A wrapper for Deeplearning4J)
=========================

This is a wrapper for DL4J API that ultimately enables the distributed training of deep neural networks over Spark 

To learn more about DL4J please visit the project's [github](https://github.com/deeplearning4j/deeplearning4j), or [website](http://deeplearning4j.org/)

---
## Using Deeplearning4Spark

this is an example of how to run dl4spark lib:

	import roireshef.spark.dl4spark.dao.DatasetUtils._
	import org.deeplearning4j.spark.canova.RecordReaderFunction
	import org.canova.api.records.reader.impl.CSVRecordReader
	import org.nd4j.linalg.dataset.DataSet
	
	//create a mapping function from Iterable[<your data type>] into a DataSet
	//This assumes a tab delimited strings RDD with first field as the class (7 possible values)
	val transformer: Iterable[String] => DataSet = 
		it => {
			val rReader = new RecordReaderFunction(new CSVRecordReader(0,"\t"), 0, 7)
			DataSet.merge(it.toList.map(rReader.call))
		}
	
	val maxBatchSize = 100
	val partitions = 12
	
	//read data and split for training (80%) and validation (20%) datasets
	val datasets = sc.textFile(...) ... .randomSplit(Array(0.8,0.2),seed)
	
	//migrate data to your favorite nd4j backend
	val trainData = datasets(0).redistribute(partitions).makeBatches(maxBatchSize,transformer)
	val validationData = datasets(1).redistribute(partitions).makeBatches(maxBatchSize,transformer)
	
	//initialize a MultiLayerNetwork object
	val conf = new NeuralNetConfiguration.Builder() ...
	val net = new MultiLayerNetwork(conf)
	net.init()
	
	//initialize a spark network and load 
	val sparkNetwork = new DL4SparkNetwork(sc, net).load(trainData, validationData)
	
	//run epochs
	val stats = for (i <- 1 to nEpochs) yield{
		sparkNetwork.runIteration()
		val trainScore = sparkNetwork.lastScore
		val testScore = sparkNetwork.calculateValidationScore
		(i, trainScore, testScore)
	}
	
	//print validation confusion matrix
	println(sparkNetwork.evaluate(validationData.mapPartitions(_.map(_._2),true)).confusionToString())