/*package roireshef.spark.dl4spark.nn.updater

import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.learning.{GradientUpdaterAggregator, GradientUpdater}
import org.nd4j.linalg.ops.transforms.Transforms

/**
 * Created by roir on 28/08/2016.
 */
class RmsProp(  val rmsDecay: Double = 0.95,
                var learningRate: Double  = 1e-1,
                var lastGradient: INDArray = null.asInstanceOf[INDArray]
               ) extends GradientUpdater{
  private val epsilon: Double = 1e-8

  def this() = this()

  override def update(args: AnyRef*): Unit = if (args.nonEmpty) learningRate = args(0).asInstanceOf[Double]
  override def getAggregator(addThis: Boolean): GradientUpdaterAggregator = ???
  override def getGradient(gradient: INDArray, iteration: Int): INDArray = {
    if (lastGradient == null)
      lastGradient = Nd4j.zeros(gradient.shape:_*).add(epsilon)
    lastGradient.muli(rmsDecay).addi(gradient.mul(gradient).muli(1 - rmsDecay))
    // lr * gradient / (sqrt(cache) + 1e-8)
    return gradient.muli(learningRate).divi(Transforms.sqrt(lastGradient, true).addi(epsilon))
  }
}

sealed class RmsPropAggregator extends GradientUpdaterAggregator{
  override def getUpdater: GradientUpdater = ???
  override def aggregate(updater: GradientUpdater): Unit = ???
  override def combine(other: GradientUpdaterAggregator): GradientUpdaterAggregator = ???
}*/
