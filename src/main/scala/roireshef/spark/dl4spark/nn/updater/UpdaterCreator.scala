/*package roireshef.spark.dl4spark.nn.updater

import org.apache.zookeeper.KeeperException.UnimplementedException
import org.deeplearning4j.nn.api.Layer
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.updater.BaseUpdater
import org.deeplearning4j.nn.updater.aggregate.UpdaterAggregator
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.learning.{RmsProp, GradientUpdater}

/**
 * Created by roir on 28/08/2016.
 */
object UpdaterCreator {
  def getUpdater(conf: NeuralNetConfiguration) = {
    import org.deeplearning4j.nn.conf.Updater._
    conf.getLayer.getUpdater match {
      case RMSPROP =>
        new BaseUpdater {
          override def init(): Unit = {}
          override def init(variable: String, gradient: INDArray, layer: Layer): GradientUpdater = {
            var rmsprop = updaterForVariable.get(variable).asInstanceOf[RmsProp]
            if (updaterForVariable.get(variable) == null) {
              rmsprop = new RmsProp(layer.conf.getLearningRateByParam(variable), layer.conf.getLayer.getRmsDecay)
              updaterForVariable.put(variable,rmsprop)
            }

            rmsprop
          }
          override def getAggregator(addThis: Boolean): UpdaterAggregator = ???
        }
      case ADADELTA => throw new UnsupportedOperationException("ADADELTA is not implemented yet") //new AdaDeltaUpdater
      case ADAGRAD => throw new UnsupportedOperationException("ADADELTA is not implemented yet") //new AdaGradUpdater
      case ADAM => throw new UnsupportedOperationException("ADADELTA is not implemented yet") //new AdamUpdater
      case NESTEROVS => throw new UnsupportedOperationException("ADADELTA is not implemented yet") //new NesterovsUpdater
      case SGD => throw new UnsupportedOperationException("ADADELTA is not implemented yet") //new SgdUpdater
      case NONE => throw new UnsupportedOperationException("ADADELTA is not implemented yet") //new NoOpUpdater
      case CUSTOM => throw new UnsupportedOperationException("Not implemented yet.")
    }
  }
}*/