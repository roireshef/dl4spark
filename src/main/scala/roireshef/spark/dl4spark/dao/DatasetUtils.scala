package roireshef.spark.dl4spark.dao

import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.util.FeatureUtil

import scala.reflect.ClassTag

/**
 * Created by Roi Reshef on July 2016.
 * This class is a utility wrapper for RDDs for easy pre processing of data and transforming it into Datasets
 * and in particular mini-batch datasets.
 */
object DatasetUtils extends Serializable{
  implicit class DataRDD[T: ClassTag](rdd:RDD[T]){
    def redistribute(partitions: Int) =
      Transformers.redistribute(rdd,partitions)
    def toDataSets(maxDSSize: Int, f: Iterable[T] => DataSet) =
      rdd.mapPartitions(part=>Mappers.makeBatches(part.toIterable,maxDSSize).toIterator).
        zipWithIndex().mapPartitions(_.map(_.swap),preservesPartitioning = true).
        mapValues(f)
  }

  implicit class LabeledPointRDD(rdd:RDD[LabeledPoint]){
    def toDataSets(numOutcomes: Int): RDD[(Long, DataSet)] = {
      rdd.mapPartitions(Mappers.fromLabeledPoint(numOutcomes)).
        zipWithIndex().mapPartitions(_.map(_.swap), preservesPartitioning = true)
    }
    def toDataSets(): RDD[(Long, DataSet)] = toDataSets(rdd.map(_.label).max.toInt + 1)
  }

  private object Transformers {
    def redistribute[T: ClassTag](rdd: RDD[T], partitions: Int) = {
      rdd.zipWithUniqueId().
        mapValues(_ % partitions).
        mapPartitions(_.map(_.swap), preservesPartitioning = true).
        aggregateByKey(Seq[T](), partitions)(_ ++ Seq(_), _ ++ _).
        flatMap(_._2)
    }
  }
  private object Mappers{
    def makeBatches[T](iter: Iterable[T], maxBatchSize: Int): Iterable[Iterable[T]] = {
      val partitions = math.ceil(1.0 * iter.size / maxBatchSize).toInt
      iter.
        zipWithIndex.
        groupBy(_._2 % partitions).
        map { case (partIdx, dsIterable) => dsIterable.map(_._1) }
    }

    def fromLabeledPoint(numOutcomes: Int) =
      (iter:Iterator[LabeledPoint]) => {
        val (iter1, iter2) = iter.duplicate
        val nRows = iter1.size
        if (iter2.nonEmpty) {
          var cur = iter2.next()
          val nCols = cur.features.size

          val featureArray = Nd4j.create(nRows, nCols)
          val labelArray = Nd4j.create(nRows, numOutcomes)

          var row = 0
          do {
            //fill feature matrix on relevant row
            for (col <- 0 until nCols) featureArray.putScalar(row, col, cur.features(col))
            labelArray.putRow(row, FeatureUtil.toOutcomeVector(cur.label.toInt, numOutcomes))
            row += 1
          } while (if (iter2.hasNext) { cur = iter2.next(); true } else false)

          Iterator(new DataSet(featureArray, labelArray))
        } else Iterator.empty
      }
  }
}
