package roireshef.spark.dl4spark.dao

import org.apache.spark.mllib.linalg
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.factory.Nd4j

/**
 * Created by roir on 04/08/2016.
 */
object INDArrayUtils {
  object Mappers{
    val fromVector: linalg.Vector => INDArray = {
      case v: linalg.SparseVector =>
        fromVector(v.toDense)
      case v: linalg.DenseVector =>
        Nd4j.create(v.values)
    }
    val fromMatrix: linalg.Matrix => INDArray = {
      case m: linalg.SparseMatrix =>
        fromMatrix(m.toDense)
      case m: linalg.DenseMatrix =>
        Nd4j.create(m.values, Array(m.numRows, m.numCols))
    }
  }
}
