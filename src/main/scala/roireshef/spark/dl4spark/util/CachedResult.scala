package roireshef.spark.dl4spark.util

import org.apache.spark.annotation.Experimental
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD

/**
  * Created by roir on 08/08/2016.
  */
@Experimental
class CachedResult[T](val value:T, val anchors: Seq[Any]) {
  require(anchors forall(x=> x.isInstanceOf[RDD[_]] || x.isInstanceOf[Broadcast[_]]),
     "cannot instantiate CacheResult instance with anchors that are neither RDD[_] nor Broadcast[_]")

  def this(value:T, rddAnchor: RDD[_]) = this(value,Seq(rddAnchor))
  def this(value:T, bcAnchor: Broadcast[_]) = this(value,Seq(bcAnchor))

   def destroy() = anchors foreach{
     case rdd: RDD[_] => rdd.unpersist()
     case bc: Broadcast[_] => bc.destroy()
   }
   def unpersist() = anchors foreach{
     case rdd: RDD[_] => rdd.unpersist()
     case bc: Broadcast[_] => bc.unpersist()
   }
}