package roireshef.spark.dl4spark.dto

import org.deeplearning4j.nn.updater.aggregate.UpdaterAggregator
import roireshef.spark.dl4spark.engine.serde.nd4j.SerINDArray

/**
 * Created by roir on 31/07/2016.
 */
case class TrainResultsAggregator(
                       params:SerINDArray,
                       accumScore:Double,
                       accumNumExamples: Long,
                       accumBatches: Int,
                       updaterAgg: UpdaterAggregator
                       ) extends Serializable
