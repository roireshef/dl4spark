package roireshef.spark.dl4spark.dto

import org.apache.spark.rdd.RDD

/**
 * Created by roir on 31/07/2016.
 */
case class Prediction[T](actualClass: T, predictionClass: T, confidence: Option[Double]) extends Serializable{
  def isTrue: Boolean = actualClass.equals(predictionClass)
}

object Prediction{
  implicit class PredictionRDD[T](rdd:RDD[Prediction[T]]){
    def topQuantile(q: Double) = {
      val n = rdd.count()
      rdd.sortBy(-_.confidence.get).zipWithIndex().
        filter(_._2 <= math.ceil(q * (n - 1)).toInt).map(_._1)
    }
  }
}