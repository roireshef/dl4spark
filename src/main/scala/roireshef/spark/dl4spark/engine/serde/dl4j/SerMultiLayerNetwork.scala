package roireshef.spark.dl4spark.engine.serde.dl4j

import roireshef.spark.dl4spark.engine.serde.DL4JSerDe
import roireshef.spark.dl4spark.engine.serde.nd4j.SerINDArray

/**
 * Created by roir on 15/08/2016.
 */
case class SerMultiLayerNetwork(params: SerINDArray, jsonConf: String) extends Serializable{
  def deserialize = DL4JSerDe.scala2dl4j(params, jsonConf)
}
