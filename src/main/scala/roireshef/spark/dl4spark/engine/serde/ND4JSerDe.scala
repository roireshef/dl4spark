package roireshef.spark.dl4spark.engine.serde

import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.factory.Nd4j
import roireshef.spark.dl4spark.engine.serde.nd4j.SerINDArray

/**
 * Created by roir on 29/07/2016.
 */
object ND4JSerDe{
  private[serde] def nd4j2scala(arr: INDArray) = (
      (for (i <- 0 to (arr.length()-1)) yield arr.getDouble(i)).toArray,
      arr.shape()
    )
  private[serde] def scala2nd4j(values: Array[Double], shape: Array[Int]) = Nd4j.create(values,shape)

  def serialize(arr: INDArray): SerINDArray = {
    val (values,shape) = nd4j2scala(arr)
    SerINDArray(values,shape)
  }
}



