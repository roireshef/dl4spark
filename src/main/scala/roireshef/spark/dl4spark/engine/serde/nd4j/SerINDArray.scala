package roireshef.spark.dl4spark.engine.serde.nd4j

import roireshef.spark.dl4spark.engine.serde.ND4JSerDe

/**
 * Created by roir on 15/08/2016.
 */
case class SerINDArray(values: Array[Double], shape: Array[Int]) extends Serializable{
  def deserialize = ND4JSerDe.scala2nd4j(values, shape)
  def isEmpty = values == null || values.length == 0
}
