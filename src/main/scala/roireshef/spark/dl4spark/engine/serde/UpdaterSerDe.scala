/*package roireshef.spark.dl4spark.engine.serde

/**
 * Created by roir on 28/08/2016.
 */
object UpdaterSerDe {
  private[serde] def nd4j2scala(updater: Updater) = {
    updater match{
      case up: RmsProp => SerRmsPropUpdater(up.rmsDecay, up.learningRate, ND4JSerDe.serialize(up.lastGradient))
      case x => throw new UnsupportedOperationException(
        s"${x.getClass.getSimpleName} is not implemented for updater serialization}")
    }
  }
  private[serde] def scala2nd4j(serUpd: SerUpdater) = {
    serUpd match{
      case sup: SerRmsPropUpdater =>
        new RmsProp(sup.rmsDecay, sup.learningRate, sup.lastGradient.deserialize)
    }
  }

  def serialize(updater: Updater): SerUpdater = nd4j2scala(updater)
}*/
