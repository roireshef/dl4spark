package roireshef.spark.dl4spark.engine.serde

import org.deeplearning4j.nn.conf.MultiLayerConfiguration
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import roireshef.spark.dl4spark.engine.serde.nd4j.SerINDArray
import roireshef.spark.dl4spark.engine.serde.dl4j.SerMultiLayerNetwork

/**
 * Created by roir on 31/07/2016.
 */
object DL4JSerDe {
  private[serde] def dl4j2scala(net: MultiLayerNetwork, backwardOnly: Boolean) = {
    (ND4JSerDe.serialize(net.params(backwardOnly)), net.getLayerWiseConfigurations.toJson)
  }
  private[serde] def scala2dl4j(params: SerINDArray, jsonConf: String) = {
    val network = new MultiLayerNetwork(MultiLayerConfiguration.fromJson(jsonConf))
    network.init()
    network.setParameters(params.deserialize)
    network
  }

  def serialize(net: MultiLayerNetwork, backwardOnly: Boolean): SerMultiLayerNetwork = {
    val (params,conf) = dl4j2scala(net, backwardOnly)
    SerMultiLayerNetwork(params, conf)
  }
}

