package roireshef.spark.dl4spark.engine

import org.apache.spark._
import org.apache.spark.annotation.Experimental
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.deeplearning4j.eval.Evaluation
import org.deeplearning4j.nn.api.Updater
import org.deeplearning4j.nn.conf.MultiLayerConfiguration
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.updater.aggregate.UpdaterAggregator
import org.deeplearning4j.nn.updater.{MultiLayerUpdater, UpdaterCreator}
import org.deeplearning4j.optimize.api.IterationListener
import org.deeplearning4j.spark.impl.multilayer.evaluation.{EvaluateFlatMapFunction, EvaluationReduceFunction}
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.factory.Nd4j
import roireshef.spark.dl4spark.dto.Prediction._
import roireshef.spark.dl4spark.dto.{Prediction, TrainResultsAggregator}
import roireshef.spark.dl4spark.engine.serde.nd4j.SerINDArray
import roireshef.spark.dl4spark.engine.serde.{DL4JSerDe, ND4JSerDe}
import roireshef.spark.dl4spark.util.CachedResult

import scala.collection.JavaConversions._

/**
 * Created by roir on 19/07/2016.
 */
class DL4SparkNetwork(sc: SparkContext, var model: MultiLayerNetwork) extends Logging{
  import DL4SparkNetwork.accumulators._

  // Data Declarations
  private var batchTrainSet: RDD[(Long,DataSet)] = _
  private var batchValidationSet: RDD[(Long,DataSet)] = _
  private var trainBatchKeys: RDD[Long] = _
  private var validationBatchKeys: RDD[Long] = _
  def getTrainSet = batchTrainSet
  def getValidationSet = batchValidationSet

  //Training Declarations
  private var _lastScore:Double = 0
  def lastScore = _lastScore
  private var iterationsDone = 0
  def getIterationsDone = iterationsDone

  //Listeners
  protected var listeners = Seq.empty[IterationListener]
  def setListeners(lst: IterationListener*) = {listeners = lst; this}

  /** Initialization **/

  def load( trainSet: RDD[(Long,DataSet)],
            validationSet: RDD[(Long,DataSet)],
            partitioner: Partitioner = new HashPartitioner(sc.defaultParallelism),
            dataStorageLevel: StorageLevel = StorageLevel.MEMORY_ONLY_SER) = {
    loadPersisted(
      trainSet.partitionBy(partitioner).persist(dataStorageLevel),
      validationSet.partitionBy(partitioner).persist(dataStorageLevel)
    )

    this
  }

  def loadPersisted( trainSet: RDD[(Long,DataSet)],
                     validationSet: RDD[(Long,DataSet)]) = {
    batchTrainSet = trainSet
    batchValidationSet = validationSet
    trainBatchKeys = batchTrainSet.keys.cache()
    validationBatchKeys = batchValidationSet.keys.cache()

    trainBatchKeys.count()
    validationBatchKeys.count()

    this
  }

  /** Training **/

  //TODO: add AccumGradient
  def runIteration(): Unit = {
    udpateTrainIntermStorage()

    if (model.getUpdater == null) model.setUpdater(UpdaterCreator.getUpdater(model))

    //broadcast current net params/conf to all workers
    val bcModelConf = sc.broadcast(model.getLayerWiseConfigurations.toJson)
    val bcNetParams = sc.broadcast(ND4JSerDe.serialize(model.params(false)))
    val bcNetUpdater = sc.broadcast(model.getUpdater)

    val counterAccum = sc.accumulator(0)
    
    val results = trainBatchKeys.mapPartitions({ iter =>
      //fetch locally stored batches from LocalStorage (singletone on each node)
      val dataSetIter = iter.map(LocalStorage.trainStorage.batches.get).flatten

      val network = new MultiLayerNetwork(MultiLayerConfiguration.fromJson(bcModelConf.value))
      network.setInitDone(true)
      network.init()

      val params = bcNetParams.value.deserialize
      val updater = bcNetUpdater.value.asInstanceOf[MultiLayerUpdater]

      dataSetIter.map { batch =>

        network.setParameters(params.dup())
        network.setUpdater(updater.clone())
        network.fit(batch)

        //return batch score and num of examples
        (ND4JSerDe.serialize(network.params(false)), network.getUpdater, network.score() * batch.numExamples(), batch.numExamples())
      }
      
    }, true)

    val aggregated = results.aggregate(
      TrainResultsAggregator(SerINDArrayAccParam.zero(null), 0, 0, 0, UpdaterAggAccParam.zero(null))
    )({
      case (agg,(params, updater,netScore, numExamples)) =>
        TrainResultsAggregator(
          SerINDArrayAccParam.addInPlace(agg.params,params),
          agg.accumScore + netScore,
          agg.accumNumExamples + numExamples,
          agg.accumBatches + 1,
          UpdaterAggAccParam.addInPlace(agg.updaterAgg,updater)
        )
    },{
      case (agg1, agg2) =>
        TrainResultsAggregator(
          SerINDArrayAccParam.addInPlace(agg1.params,agg2.params),
          agg1.accumScore + agg2.accumScore,
          agg1.accumNumExamples + agg2.accumNumExamples,
          agg1.accumBatches + agg2.accumBatches,
          UpdaterAggAccParam.addInPlace(agg1.updaterAgg,agg2.updaterAgg)
        )
    })

    val maxRep = aggregated.accumBatches

    val newParams = aggregated.params.deserialize.divi(maxRep)
    model.setParameters(newParams)
    
    val meanScore = aggregated.accumScore / aggregated.accumNumExamples
    
    val combinedUpdater = aggregated.updaterAgg.getUpdater
    model.setUpdater(combinedUpdater)
    
    model.setScore(meanScore)
    _lastScore = meanScore

    iterationsDone+=1
    invokeListeners(iterationsDone)

    bcModelConf.destroy()
    bcNetParams.destroy()
    bcNetUpdater.destroy()
  }
  def validationScore: Double = {
    udpateValidationIntermStorage()

    val bcNet = sc.broadcast(DL4JSerDe.serialize(model, false))

    val scores = validationBatchKeys.mapPartitions({ iter =>
      //fetch locally stored batches from LocalStorage (singleton on each node)
      val dataSetIter = iter.
        map(LocalStorage.validationStorage.batches.get).flatten.
        filter(x=> x.nonEmpty && x.numExamples() > 0)

      if (dataSetIter.hasNext) {
        val mergedDS = DataSet.merge(dataSetIter.toList)

        val network = bcNet.value.deserialize

        //return batch score and num of examples
        Iterator((network.score(mergedDS, false) * mergedDS.numExamples(), mergedDS.numExamples()))
      } else Iterator((0d,0))
    }, true)

    val (sumScores, numEx) = scores.reduce{case ((sc1, numEx1), (sc2, numEx2)) => (sc1+sc2,numEx1+numEx2)}

    bcNet.destroy()

    sumScores / numEx
  }
  def calculateScore(data: RDD[DataSet], average: Boolean) = {
    val crScores = calculateScores(data)
    val result = if (average)
      crScores.value.sum() / data.map(_.numExamples()).sum()
    else
      crScores.value.sum()
    
    crScores.destroy()
  }
  
  def calculateScores(data: RDD[DataSet]): CachedResult[RDD[Double]] = {
    val bcNet = sc.broadcast(DL4JSerDe.serialize(model, false))

    val scores = data.mapPartitions({ iter =>
      val network = bcNet.value.deserialize

      //return batch score and num of examples
      iter.map(ds=> network.score(ds, false) * ds.numExamples())
    }, true)

    new CachedResult(scores, bcNet)
  }

  protected def invokeListeners(iteration: Int) = {
    if (listeners.nonEmpty)
      listeners.foreach{ listener =>
        try{
          listener.iterationDone(model, iteration)
        } catch{
          case e:Exception => logError("Exception caught at IterationListener invocation", e)
        }
      }
  }

  /** Prediction & Evaluation **/

  def predict(data: RDD[DataSet], withConfidence: Boolean = true): CachedResult[RDD[Prediction[String]]] = {
    val bcNet = sc.broadcast(DL4JSerDe.serialize(model, false))

    val predictions = if (withConfidence) {
      data.mapPartitions({ iter =>
        val network = bcNet.value.deserialize

        //return batch score and num of examples
        iter.flatMap { ds =>
          val actLabels = for (i <- 0 to (ds.numExamples()-1)) yield
            ds.getLabelName(Nd4j.getBlasWrapper.iamax(ds.getLabels.getRow(i)))
          val predLabels = network.predict(ds)

          val maxProbs = network.feedForward(ds.getFeatureMatrix).toList.last.max(1)

          val confidences = for (i <- 1 to (maxProbs.length() - 1)) yield maxProbs.getDouble(i)
          (for ((act, pred, conf) <- (actLabels, predLabels, confidences).zipped)
            yield Prediction(act, pred, Some(conf))).toIterator
        }
      }, true)
    } else {
      data.mapPartitions({ iter =>
        val network = bcNet.value.deserialize

        //return batch score and num of examples
        iter.flatMap { ds =>
          val actLabels = for (i <- 0 to (ds.numExamples()-1)) yield
            ds.getLabelName(Nd4j.getBlasWrapper.iamax(ds.getLabels.getRow(i)))
          val predLabels = network.predict(ds)

          (for ((act, pred) <- (actLabels, predLabels).zipped)
            yield Prediction(act, pred, None)).toIterator
        }
      }, true)
    }

    new CachedResult(predictions, bcNet)
  }

  def evaluate(data: RDD[DataSet]): Evaluation = evaluate(data, null)
  def evaluate(data: RDD[DataSet], labelsList: List[String]): Evaluation = evaluate(data, labelsList, DL4SparkNetwork.DEFAULT_EVAL_SCORE_BATCH_SIZE)
  def evaluate (data: RDD[DataSet], labelsList: List[String], evalBatchSize: Int): Evaluation = {
    val bcNetParams = sc.broadcast(model.params(false))
    val bcNetConf = sc.broadcast(model.getLayerWiseConfigurations.toJson)

    val listBroadcast = if (labelsList == null) null else sc.broadcast[java.util.List[String]](labelsList)
    val evaluation = data.
      mapPartitions(new EvaluateFlatMapFunction(bcNetConf, bcNetParams, evalBatchSize, listBroadcast).call(_).toIterator).
      reduce(new EvaluationReduceFunction().call)
    
    bcNetParams.destroy()
    bcNetConf.destroy()

    evaluation
  }

  @Experimental
  def getMultiClassMetrics(data: RDD[DataSet], topQuantile: Double = 1d) = {
    val dataWasCachedBefore = data.getStorageLevel != StorageLevel.NONE

    if (!dataWasCachedBefore) data.cache()

    val crPredictions = predict(data)
    
    val predictions = if (topQuantile < 1)
      crPredictions.value.topQuantile(topQuantile)
    else
      crPredictions.value

    val first = data.first()
    val map = (for (i <- 0 until first.numOutcomes()) yield (first.getLabelName(i), i)).toMap.map(x=>x)
    val bcMap = sc.broadcast(map)

    val numericPredictions = predictions.map( prediction =>
      (bcMap.value(prediction.predictionClass).toDouble, bcMap.value(prediction.actualClass).toDouble) )

    new CachedResult(
      new MulticlassMetrics(numericPredictions),
      Seq(bcMap) ++ crPredictions.anchors ++ (if(!dataWasCachedBefore) Seq(data) else Nil)
    )
  }

  /** LOCAL STORAGE MANAGEMENT **/

  def clear(): Unit = {
    if (!(trainBatchKeys == null || trainBatchKeys.isEmpty())){
      trainBatchKeys.foreachPartition(_=>LocalStorage.trainStorage.clear())
      batchTrainSet.unpersist()
      trainBatchKeys = null
      batchTrainSet = null
    }
    if (!(validationBatchKeys == null || validationBatchKeys.isEmpty())){
      validationBatchKeys.foreachPartition(_=>LocalStorage.validationStorage.clear())
      batchValidationSet.unpersist()
      validationBatchKeys = null
      batchValidationSet = null
    }
  }
  private def udpateTrainIntermStorage() = {
    val missingBatchesIds = trainBatchKeys.
      mapPartitions(x=>LocalStorage.trainStorage.getMissing(x.toIterable).toIterator,true).
      cache()

    if (missingBatchesIds.count()>0)
      missingBatchesIds.
        mapPartitions(_.map((_,null)),true).
        join(batchTrainSet).
        foreach{ case (idx,(_,iter)) =>
          LocalStorage.trainStorage.update(idx,iter)
        }

    missingBatchesIds.unpersist()
  }
  private def udpateValidationIntermStorage() = {
    val missingBatchesIds = validationBatchKeys.
      mapPartitions(x=>LocalStorage.validationStorage.getMissing(x.toIterable).toIterator,true).
      cache()

    if (missingBatchesIds.count()>0)
      missingBatchesIds.
        mapPartitions(_.map((_,null)),true).
        join(batchValidationSet).
        foreach{ case (idx,(_,iter)) =>
          LocalStorage.validationStorage.update(idx,iter)
        }

    missingBatchesIds.unpersist()
  }
}

object DL4SparkNetwork{
  private val DIVIDE_ACCUM_GRADIENT = "org.deeplearning4j.spark.iteration.dividegrad"
  private val ACCUM_GRADIENT = "org.deeplearning4j.spark.iteration.accumgrad"
  private val DEFAULT_EVAL_SCORE_BATCH_SIZE = 50

  private object accumulators{
    object SerINDArrayAccParam extends AccumulatorParam[SerINDArray]{
      override def addInPlace(r1: SerINDArray, r2: SerINDArray): SerINDArray = {
        (r1, r2) match{
          case (SerINDArray(null,null), SerINDArray(null,null)) => SerINDArray(null,null)
          case (SerINDArray(null,null), nonEmptyScalaINDArray) => nonEmptyScalaINDArray
          case (nonEmptyScalaINDArray, SerINDArray(null,null)) => nonEmptyScalaINDArray
          case (SerINDArray(values1,shape1), SerINDArray(values2,shape2)) if shape1.deep == shape2.deep =>
            SerINDArray(values1 zip values2 map Function.tupled(_+_), shape1)
          case (SerINDArray(values1,shape1), SerINDArray(values2,shape2)) =>
            throw new java.lang.UnsupportedOperationException(
              s"array with shape ${shape2.deep} can be added to array with shape ${shape1.deep}"
            )
        }
      }
      override def zero(initialValue: SerINDArray): SerINDArray = SerINDArray(null,null)
    }
    object UpdaterAggAccParam extends AccumulatorParam[UpdaterAggregator]{
      def addInPlace(r1: UpdaterAggregator, r2: Updater): UpdaterAggregator = {
        (r1, r2) match{
          case (null, null) => null
          case (null, updater) => updater.getAggregator(true)
          case (upAgg, null) => upAgg
          case (upAgg, updater) =>
            upAgg.aggregate(updater)
            upAgg
        }
      }
      override def addInPlace(r1: UpdaterAggregator, r2: UpdaterAggregator): UpdaterAggregator = {
        (r1, r2) match{
          case (nonEmptyUpdaterAgg, null) => nonEmptyUpdaterAgg
          case (null, nonEmptyUpdaterAgg) => nonEmptyUpdaterAgg
          case (upAgg1, upAgg2) =>
            upAgg1.merge(upAgg2)
            upAgg1
        }
      }
      override def zero(initialValue: UpdaterAggregator): UpdaterAggregator = null
    }
  }
}







