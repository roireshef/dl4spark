package roireshef.spark.dl4spark.engine

import org.nd4j.linalg.dataset.DataSet

import scala.collection.JavaConversions._
import scala.collection.concurrent
/**
 * Created by Roi Reshef on July 2016.
 * LocalStorage is a (per-node) singleton that manages batch storage for nd4j backend (dataset objects)
 */

private [engine]
object LocalStorage{
  protected trait LocalStorage {
    val batches: concurrent.TrieMap[Long,DataSet]
    def update(batchIdx: Long, batchData: DataSet):Unit = batches += ((batchIdx, batchData))
    def getMissing(idxs: Set[Long]) = idxs.filterNot(batches.keys.contains)
    def getMissing(idxs: Iterable[Long]):Iterable[Long] = getMissing(idxs.toSet)
    protected [engine] def clear() = if (batches.nonEmpty) batches.clear()
  }
  object trainStorage extends LocalStorage{
    @transient lazy val batches = concurrent.TrieMap[Long,DataSet]()
  }
  object validationStorage extends LocalStorage{
    @transient lazy val batches = concurrent.TrieMap[Long,DataSet]()
  }
}