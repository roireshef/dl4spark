import sbt.Keys._

name := "dl4spark"

version := "1.0"

scalaVersion := "2.11.8"
//override scala version
ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

val nd4jBackend = "nd4j-native" //"nd4j-cuda-7.5"

val nd4jVersion = "0.4.0"
val dl4jVersion = "0.4.0"
val canovaVersion = "0.0.0.16"
val guavaVersion = "19.0"
//val arbiterVersion = "0.0.0.7"

val dl4jPath = s"http://repo1.maven.org/maven2/org/deeplearning4j/"+
  s"dl4j-spark_$scalaBinaryVersion/$dl4jVersion/"+
  s"dl4j-spark_$scalaBinaryVersion-$dl4jVersion.jar"

val kryoPath = /*s"http://repo1.maven.org/maven2/org/nd4j/"+
  s"nd4j-kryo__$scalaBinaryVersion/$nd4jVersion/"+
  s"nd4j-kryo_$scalaBinaryVersion-$nd4jVersion.jar"*/
  "http://central.maven.org/maven2/org/nd4j/nd4j-kryo_2.11/0.4.0/nd4j-kryo_2.11-0.4.0.jar"

classpathTypes += "maven-plugin"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.0.2" % "provided",
  "org.apache.spark" %% "spark-mllib" % "2.0.2" % "provided",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test",

  "org.deeplearning4j" % "deeplearning4j-core" % dl4jVersion,
//  "org.deeplearning4j" % "deeplearning4j-ui" % dl4jVersion,
  "org.deeplearning4j" %% "dl4j-spark" % dl4jVersion from dl4jPath,

  "org.nd4j" % "canova-nd4j-codec" % canovaVersion,

  "com.google.guava" % "guava" % guavaVersion,
  "org.bytedeco" % "javacpp" % "1.2.1",

  "org.nd4j" % nd4jBackend % nd4jVersion classifier "" classifier "windows-x86_64",
//  "org.nd4j" % nd4jBackend % nd4jVersion classifier "" classifier "linux-x86_64",
  "org.nd4j" %% "nd4j-kryo" % nd4jVersion from kryoPath
)

dependencyOverrides ++= Set(
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.4.4",
  "com.fasterxml.jackson.core" % "jackson-core" % "2.4.4",
  "com.fasterxml.jackson.core" % "jackson-annotations" % "2.4.4",
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-yaml" % "2.4.4"
)

excludeDependencies ++= Seq(
  SbtExclusionRule(organization = "com.github.fommil.netlib"),
  SbtExclusionRule(organization = "org.apache.parquet")
)